#!/usr/bin/env bash
source .env

# git cloning Casper theme as a git submodule
git submodule update


# set up git repository for GITLAB (check if there is a remote first, if not create new repo)

git_clone_status=0
if [[ ! -d static ]];then

	echo "cloning $GIT_REMOTE"
	git clone $GIT_REMOTE static
	git_clone_status=$?

	if [[ ! $git_clone_status -eq 0 ]];then
		echo "remote repo does not exist, so create directory structure, GitLab CI config and dummy index file"
		mkdir -p static/public
		cp sample-gitlab-ci.yml static/.gitlab-ci.yml
		cp sample-readme.md static/README.md
		test -f "static/public/index.html" || cp index.html static/public/

		echo "Initialising a new repo for static to $GIT_REMOTE"
		pushd static
		if [ ! -d .git ]; then
			git init
			git add -A
			git commit -m "Initialising the static directory"
			git push --set-upstream $GIT_REMOTE master
			git remote add origin $GIT_REMOTE
		fi
		popd
		echo "successfully created a new repo for static at $GIT_REMOTE"
	else
		echo "cloning existing $GIT_REMOTE to static"
	fi
else
	echo "static directory already exists, no action taken"	
fi


